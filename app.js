const express = require("express")
const path = require('path')
const app = express()
const userRoutes = require('./routes/userRoutes')
const viewRoutes = require('./routes/viewRoutes')

app.use(express.json())
app.use('/api/v1/users', userRoutes)
app.use('/', viewRoutes)
app.use(express.static(path.join(__dirname, 'views')))

module.exports = app 