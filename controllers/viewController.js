const path = require('path')

//Login page
exports.getLoginForm = (req, res) => {
    res.sendFile(path.join(__dirname, '../', 'views', 'login.html'))
}

//Sign up page
exports.getSignupForm = (req, res) => {
    res.sendFile(path.join(__dirname, '../', 'views', 'signup.html'))
}

//Home page
exports.getHome = (req, res) => {
    res.sendFile(path.join(__dirname, '../', 'views', 'index.html'))
}