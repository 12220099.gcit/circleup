// const app = require('./app')
// const dotenv = require('dotenv');
// const mongoose = require('mongoose');
// dotenv.config();

const mongoose = require('mongoose');
const dotenv = require('dotenv');
dotenv.config({path: './config.env' })
const app = require('./app')

const port = 4001

const DB = process.env.DATABASE.replace(
    "PASSWORD",
    process.env.PASSWORD
)

mongoose.connect(DB)
.then(() => {
    console.log("DB successfully connected!")
})
.catch(error => {
    console.log(error.message);
})

app.listen(port, ()=>{
    console.log(`App running on port ${port} ..`)
})